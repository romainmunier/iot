/*
  IMPORT DES MODULES
 */
const { io, http, express, app } = require("./modules/SocketIO")

/*
  IMPORT DES FICHIERS DE CONFIGURATION
 */
const ExpressConfig = require('./config/express.json');

const bodyParser = require("body-parser");
const cors = require("cors");
const morgan = require("morgan");

app.use(express.json());
app.use(cors("*"));
app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

const ExpressSession = require('express-session')({
  secret: 'secret',
  resave: false,
  saveUninitialized: false,
});

app.use(ExpressSession);

const PassportJS = require('passport');
app.use(PassportJS.initialize());
app.use(PassportJS.session());

/*
  IMPORT DES ROUTES EXTERNES
 */
const LevelAPI = require('./routes/Level');
app.use('/level/', LevelAPI);

const ParamsAPI = require('./routes/Params');
app.use('/params/', ParamsAPI);

const AuthenticateAPI = require('./routes/Authenticate');
app.use('/auth/', AuthenticateAPI);

/*
  LANCEMENT DU SERVEUR
 */
http.listen(ExpressConfig.PORT, () => {
  console.log(`Serveur is listening on port ${ExpressConfig.PORT}`);
});

io.on("connection", (socket) => {
  console.log("Connected from : " + socket.client.conn.remoteAddress)
})

io.on("disconnected", (socket) => {
  console.log("Disconnected from : " + socket.client.conn.remoteAddress)
})
