const express = require('express');
const CORS = require('cors');
const morgan = require('morgan');

const app = express();
const bodyParser = require('body-parser');

app.use(CORS('*'));
app.use(morgan('dev'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

module.exports = { app };
