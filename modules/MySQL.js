const MySQL = require("mysql")
const MySQLConfig = require("../config/mysql.json")

let connection = MySQL.createConnection({
  host     : MySQLConfig.HOST,
  user     : MySQLConfig.USERNAME,
  password : MySQLConfig.PASSWORD,
  database : MySQLConfig.DATABASE,
  port     : MySQLConfig.PORT
});

function query(REQUEST = "", PARAMS = []) {
  return new Promise((resolve, reject) => {
    connection.query(REQUEST, PARAMS, (error, results, fields) => {
      if (error) reject(error)
      else resolve(results)
    })
  })
}

module.exports = { connection, query }
