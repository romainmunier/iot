const express = require('express');
const router = express.Router();

const { io } = require("../modules/SocketIO")
const MySQL = require("../modules/MySQL")

router.post("/", async (request, response) => {
  const values = {
    date: new Date(),
    value: request.body.data
  }

  io.emit("receiveData", values)
  await MySQL.query("INSERT INTO RECORDS VALUES (NULL, ?, ?)", [values.date, values.value])

  let level = await MySQL.query("SELECT value FROM PARAMS WHERE param = ? LIMIT 1", ['level'])

  response.json({
    level: parseInt(level[0].value)
  })
})

router.post("/getBetweenDate", async (request, response) => {
  const results = await MySQL.query("SELECT * FROM RECORDS WHERE datetime BETWEEN ? AND ? ORDER BY datetime ASC", [request.body.startDate, request.body.endDate])
  response.json(results)
})

module.exports = router;
