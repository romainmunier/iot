const express = require('express');
const router = express.Router();

const JWTConfig = require('../config/jwt.json');

const { io } = require("../modules/SocketIO")
const MySQL = require("../modules/MySQL")

/*
  CONFIGURATION DE PASSPORT-JWT
 */
const PassportJS = require('passport');

const Bcrypt = require('bcrypt');
const JWT = require('jsonwebtoken');
const LocalStrategy = require('passport-local').Strategy;

PassportJS.serializeUser((user, done) => {
  done(null, user);
});

PassportJS.deserializeUser(async (user, done) => {
  let response = await MySQL.query("SELECT * FROM USERS WHERE username = ?", user.login)
  if (response.length > 0) response = response[0]
  done(false, response);
});

const JWTStrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;

PassportJS.use(
  new JWTStrategy(
    {
      secretOrKey: JWTConfig.PRIVATE_KEY,
      jwtFromRequest: ExtractJWT.fromUrlQueryParameter('token'),
    },
    async (token, done) => {
      try {
        return done(null, token.user);
      } catch (error) {
        done(error);
      }
    },
  ),
);

PassportJS.use('login', new LocalStrategy({
  usernameField: 'login',
  passwordField: 'password',
}, (login, password, done) => {
  MySQL.query("SELECT * FROM USERS WHERE username = ?", login).then((response) => {
    if (!response || response.length === 0) {
      console.log('NO USER FOUND');
      return done(null, false, { message: 'NO USER FOUND' });
    }

    response = response[0]

    console.log(password, response.password)

    if (!Bcrypt.compareSync(password, response.password)) {
      console.log('INVALID PASSWORD');
      return done(null, false, { message: 'INVALID PASSWORD' });
    }

    return done(null, response);
  })
}));

router.post('/login', async (request, response, next) => {
  PassportJS.authenticate(
    'login',
    async (err, user) => {
      try {
        if (err || !user) {
          const error = new Error('An error occurred.');

          return next(error);
        }

        request.login(
          user,
          { session: false },
          async (error) => {
            if (error) return next(error);

            console.log(user)

            const jsonObject = {
              user: {
                id: user.id,
                login: user.username,
                rights: user.rights,
              },
            };
            const token = JWT.sign(jsonObject, JWTConfig.PRIVATE_KEY);

            return response.json({ token });
          },
        );
      } catch (error) {
        return next(error);
      }
    },
  )(request, response, next);
});

router.post('/register', async (request, response, next) => {
  if (request.body.login && request.body.password) {
    Bcrypt.hash(request.body.password, 10).then(async function(result) {
      await MySQL.query("INSERT INTO USERS(username, password) VALUES (?, ?)", [request.body.login, result])
      response.send(true)
    })
  } else {
    response.send(false)
  }
});

module.exports = router;
