const express = require('express');
const router = express.Router();

const { io } = require("../modules/SocketIO")
const MySQL = require("../modules/MySQL")

router.get("/", async (request, response) => {
  let result = await MySQL.query("SELECT value FROM PARAMS WHERE param = ?", ['level']);

  response.json({
    level: result[0].value
  })
})

router.post("/", async (request, response) => {
  await MySQL.query("UPDATE PARAMS SET value = ? WHERE param = ?", [request.body.level, 'level']);

  response.send(true)
})

module.exports = router;
